import os

A2_PROFILE_CAN_CHANGE_EMAIL = False
A2_PROFILE_CAN_EDIT_PROFILE = False
A2_CAN_RESET_PASSWORD = False
REGISTRATION_OPEN = False
A2_REGISTRATION_CAN_CHANGE_PASSWORD = False
A2_REGISTRATION_CAN_DELETE_ACCOUNT = False

SAML_SIGNATURE_PUBLIC_KEY = file('/etc/authentic2/cert.pem').read()
SAML_SIGNATURE_PRIVATE_KEY = file('/etc/authentic2/key.pem').read()

LDAP_AUTH_SETTINGS = [
    {
        'url': os.environ['SUPANN_LDAP_URL'],
        'user_filter': '(&(|(mail=%s)(supannAutreMail=%s)(supannAliasLogin=%s)(uid=%s))(objectClass=supannPerson))',
        'basedn': os.environ['SUPANN_LDAP_BASE_DN'],
        'binddn': os.environ.get('SUPANN_LDAP_BINDDN'),
        'bindpw': os.environ.get('SUPANN_LDAP_BINDPW'),
        'groupsu': 'cn=admin,ou=groups,%s' % os.environ['SUPANN_LDAP_BASE_DN'],
        'groupstaff': 'cn=admin,ou=groups,%s' % os.environ['SUPANN_LDAP_BASE_DN'],
        'transient': False,
        'username_template': '{uid[0]}',
        'external_id_tuples': (('uid',), ('dn:noquote',), ),
        'lookups': ('external_id',),
        'update_username': False,
        'attributes': [
            'uid',
            'eduPersonPrincipalName',
            'eduPersonOrgDN',
            'eduPersonOrgUnitDN',
            'eduPersonPrimaryOrgUnitDN',
            'supannAliasLogin',
            'supannRefId',
            'supannCivilite',
            'givenName',
            'sn',
            'cn',
            'displayName',
            'eduPersonNickname',
            'userPassword',
            'description',
            'eduPersonAffiliation',
            'eduPersonPrimaryAffiliation',
            'supannActivite',
            'supannCodeINE',
            'supannEmpCorps',
            'supannEmpId',
            'supannEntiteAffectation',
            'supannEntiteAffectationPrincipale',
            'supannEtablissement',
            'supannEtuAnneeInscription',
            'supannEtuCursusAnnee',
            'supannEtuDiplome',
            'supannEtuElementPedagogique',
            'supannEtuEtape',
            'supannEtuId',
            'supannEtuInscription',
            'supannEtuRegimeInscription',
            'supannEtuSecteurDisciplinaire',
            'supannEtuTypeDiplome',
            'supannParrainDN',
            'supannRoleEntite',
            'supannRoleGenerique',
            'supannTypeEntiteAffectation',
            'preferredLanguage',
            'telephoneNumber',
            'supannAutreTelephone',
            'mobile',
            'facsimileTelephoneNumber',
            'mail',
            'supannAutreMail',
            'mailForwardingAddress',
            'supannMailPerso',
            'labeledURI',
            'userCertificate',
            'postalAddress',
            'physicalDeliveryOfficeName',
            'supannListeRouge' 
        ],
        'attribute_mappings': (('mail', 'email'),),
        'mandatory_attributes_values': {
                # edugain support
                'schacHomeOrganization': [os.environ['EDUGAIN_SCHAC_HOME_ORGANIZATION']],
                'schacHomeOrganizationtype': [os.environ['EDUGAIN_SCHAC_HOME_ORGANIZATION_TYPE']],
        },
    }
]
AUTHENTICATION_BACKENDS = ('authentic2.backends.LDAPBackend',)
