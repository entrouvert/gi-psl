#!/bin/sh
# Do initial configuration of slapd

set -e

BASEDIR=`dirname $0`

service slapd stop || true
BACKUPDIR="/var/backup/reset-ldap-`date +%Y%m%dT%H:%M:%S`"
mkdir -p "$BACKUPDIR"
echo Old configuration saved in $BACKUPDIR
cp -R /var/lib/ldap /etc/ldap/slapd.d/ "$BACKUPDIR"
rm -rf /var/lib/ldap/* /etc/ldap/slapd.d/*
mkdir /var/lib/ldap/config-accesslog/

echo Load overlay modules, configure auditlog for cn=config, create cn=monitor db
slapadd -n0 -F/etc/ldap/slapd.d -l"$BASEDIR/config.ldif"
slapadd -n0 -F/etc/ldap/slapd.d -l"/etc/ldap/schema/core.ldif"
slapadd -n0 -F/etc/ldap/slapd.d -l"/etc/ldap/schema/cosine.ldif"
slapadd -n0 -F/etc/ldap/slapd.d -l"/etc/ldap/schema/inetorgperson.ldif"
slapadd -n0 -F/etc/ldap/slapd.d -l"$BASEDIR/supann-2009.ldif"
slapadd -n0 -F/etc/ldap/slapd.d -l"$BASEDIR/eduperson.ldif"
slapadd -n0 -F/etc/ldap/slapd.d -l"$BASEDIR/eduorg-200210-openldap.ldif"

chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

service slapd start
