#!/bin/sh

BASEDIR=`dirname $0`

cp "$BASEDIR/ldapvirc" $HOME/.ldapvirc
apt-get install slapd ldap-utils ldapvi vim make
