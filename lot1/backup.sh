#!/bin/sh
# Backup all ldap databases to $1/<data-iso>/

set -e

DEST=$1
shift

if [ ! -d "$DEST" ]; then
	echo First argument must be an existing directory
	exit 1
fi

BACKUPDIR="$DEST/`date +%Y%m%dT%H:%M:%S`"
mkdir -p "$BACKUPDIR"
slapcat -n0 -l"$BACKUPDIR/config.ldif"
for i in `seq 1 20`; do
	echo slapcat -n$i -l"$BACKUPDIR/db-$i.ldif"
	if ! slapcat -n$i -l"$BACKUPDIR/db-$i.ldif"; then
		true
	fi
done
find "$BACKUPDIR" -size 0 -delete
