#!/bin/sh
# Restore all ldap database from $1
# Prerequisite: all ldap database are in /var/lib/ldap

set -e

BACKUPDIR=$1
shift
echo "Beware all current data will be erased, are you sure ? (type yes)"
read ok

if [ "x$ok" != "xyes" ]; then
	exit 0
fi

if [ ! -d "$BACKUPDIR" -o ! -f "$BACKUPDIR/config.ldif" ]; then
	echo First argument must be a directory containing at least a config.ldif file
	exit 1
fi

service slapd stop

echo Erasing current datas...
rm -rf /etc/ldap/slapd.d/*
find /var/lib/ldap -type f -delete

echo Loading config backups...
slapadd -n0 -F/etc/ldap/slapd.d/ -l"$BACKUPDIR/config.ldif"

echo Loading data backups...
for i in `seq 1 30`; do
	if [ -f "$BACKUPDIR/db-$i.ldif" ]; then
		slapadd -n$i -F/etc/ldap/slapd.d/ -l"$BACKUPDIR/db-$i.ldif"
	fi
done

chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/

service slapd start
