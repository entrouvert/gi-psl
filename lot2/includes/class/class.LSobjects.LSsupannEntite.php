<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

/**
 * Objet Ldap supannEntite
 *
 * @author Benjamin Renard <brenard@easter-eggs.com>
 */
class LSsupannEntite extends LSldapObject {

  var $entiteObjectType = 'LSsupannEntite';
  var $parentAttr = 'supannCodeEntiteParent';
  var $parentAttrValue = 'supannCodeEntite';

  /* ========== Children entities ========== */
  function getEntiteKeyValue($object) {
    return $this -> getObjectKeyValueInRelation($object,$this -> parentAttr,$this -> entiteObjectType);
  }
  
  function listChildren($object) {
    return $this -> listObjectsInRelation($object,$this -> parentAttr,$this -> entiteObjectType,$this -> parentAttrValue);
  }

  function addOneChild($object) {
    return $this -> addOneObjectInRelation($object,$this -> parentAttr, $this -> entiteObjectType,$this -> parentAttrValue,'canEditChildren');
  }
  
  function deleteOneChild($object) {
    return $this -> deleteOneObjectInRelation($object,$this -> parentAttr,$this -> entiteObjectType,$this -> parentAttrValue,'canEditChildren');
  }
  
  function renameOneChildren($object,$oldDn) {
    return $this -> renameOneObjectInRelation($object,$oldDn,$this -> parentAttr,$this -> entiteObjectType,$this -> parentAttrValue);
  }
  
  function updateChildren($object,$listDns) {
    return $this -> updateObjectsInRelation($object,$listDns,$this -> parentAttr,$this -> entiteObjectType,$this -> parentAttrValue,'canEditChildren');
  }

  function canEditChildren($dn=NULL) {
    if (!$dn) {
      $dn=$this -> dn;
    }
    return LSsession :: canEdit($this -> type_name,$this -> dn,$this -> parentAttr);
  }

}

?>
