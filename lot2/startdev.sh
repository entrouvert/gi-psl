#!/bin/sh

BASEDIR=`dirname $0`

echo deb http://ldapsaisie.easter-eggs.org/debian wheezy main >/etc/apt/sources.list.d/ldapsaisie.list
cp "$BASEDIR/../lot1/ldapvirc" $HOME/.ldapvirc
apt-get update
apt-get install ldap-utils ldapvi vim make ldapsaisie
