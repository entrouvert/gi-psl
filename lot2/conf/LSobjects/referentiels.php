<?php

/**
 * Copyright (C) 2014 Entr'ouvert
 */

$BCN_N_CORPS = array();

$liste_affiliations = array(
	"student" => "student",
        "faculty" => "faculty",
        "staff" => "staff",
        "employee" => "employee",
        "member" => "member",
        "affiliate" => "affiliate",
        "alum" => "alum",
        // "library-walk-in" => "library-walk-in",
        "researcher" => "researcher",
        "retired" => "retired",
        "emeritus" => "emeritus",
        "teacher" => "teacher",
        "registered-reader" => "registered-reader",
);

$liste_preferredLanguage = array();
