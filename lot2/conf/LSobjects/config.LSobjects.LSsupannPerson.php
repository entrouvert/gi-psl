<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.

******************************************************************************/

require('referentiels.php');

$GLOBALS['LSobjects']['LSsupannPerson'] = array (
	'objectclass' => array(
		'inetOrgPerson',
		'eduPerson',
		'supannPerson'
	),
	'rdn' => 'uid',
	'container_dn' => 'ou=people',

	'LSaddons' => array (
		'supann',
	),

	//"disable_creation" => true,

	'display_name_format' => '%{displayName}',
	'label' => 'Users',
	'displayAttrName' => true,
	
	// LSrelation
	'LSrelation' => array(
		'groups' => array(
			'label' => 'Belongs to groups ...',
			'emptyText' => "Doesn't belong to any group.",
			'LSobject' => 'LSsupannGroup',
			'list_function' => 'listUserGroups',
			'getkeyvalue_function' => 'getMemberKeyValue',
			'update_function' => 'updateUserGroups',
			'remove_function' => 'deleteOneMember',
			'rename_function' => 'renameOneMember',
			'canEdit_function' => 'canEditGroupRelation',
			'canEdit_attribute' => 'memberUid',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			)
		)
	),
	
	// LSform
	'LSform' => array (
		'ajaxSubmit' => 1,
		// Layout
		'layout' => array (
			'Civility' => array(
				'label' => 'Civility',
				'img' => 1, 
				'args' => array (
					'uid',
					'eduPersonPrincipalName',
					'supannAliasLogin',
					'supannRefId',
					'supannCivilite',
					'givenName',
					'sn',
					'cn',
					'displayName',
					'eduPersonNickname',
					'userPassword',
					'description',
					'jpegPhoto'
				)
			),
			'school' => array (
				'label' => 'School',
				'args' => array (
					'eduPersonAffiliation',
					'eduPersonPrimaryAffiliation',
					'supannActivite',
					'supannEtuId',
					'supannCodeINE',
					'supannEmpId',
					'supannEmpCorps',
					'supannEntiteAffectation',
					'supannEntiteAffectationPrincipale',
					'supannRoleEntite',
					'supannRoleGenerique',
					'supannTypeEntiteAffectation',
					'supannEtablissement',
					'supannParrainDN',
				)
			),
			'communication' => array (
				'label' => 'Communication',
				'args' => array (
					'preferredLanguage',
					'telephoneNumber',
					'supannAutreTelephone',
					'mobile',
					'facsimileTelephoneNumber',
					'mail',
					'supannAutreMail',
					'mailForwardingAddress',
					'supannMailPerso',
					'labeledURI',
					'userCertificate',
					'postalAddress',
					'physicalDeliveryOfficeName',
					'supannListeRouge',
				)
			),
		), // fin Layout & fin array_merge
	), // fin LSform
	
	'LSsearch' => array (
		'attrs' => array (
			'givenName',
			'sn',
			'cn',
			'uid',
			'mail',
			'displayName',
			'supannMailPerso'
		),
		'params' => array (
			'sortBy' => 'displayName',
			'sizelimit' => 300
		),
	),

	// Attributes
	'attrs' => array (
	
		/* ----------- start -----------*/
		'uid' => array (
			'label' => 'Identifier',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'check_data' => array (
				'alphanumeric' => array(
					'msg' => "The identifier must contain only letters or numbers."
				),
			),
			'validation' => array (
				array (
					'filter' => '(|(uid=%{val})(supannAliasLogin=%{val}))',
					'result' => 0,
					'msg' => 'This identifier is already used.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array (
				'eduPersonPrincipalName'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'userPassword' => array (
			'label' => 'Password',
			'ldap_type' => 'password',
			'html_type' => 'password',
			'html_options' => array(
				'generationTool' => true,
				'autoGenerate' => false,
				'lenght' => 8,
				'chars' => array (
					array (
						'chars' => '23456789abcdefjkpqrstwxyzABCDEFJKPQRSTWXYZ',
						'nb' => 6
					)
				),
			),
			'check_data' => array(
				'password' => array(
					'msg' => 'The password length must be between 7 and 10 characters.',
					'params' => array(
						'minLength' => 7,
						'maxLength' => 10
					)
				)
			),
			'required' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1,
				'lostPassword' => 1
			),
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonPrincipalName' => array (
			'label' => 'Identifier single institutional',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
                        'generate_value_format' => '%{uid}@' . LS_SUPANN_EPPN_DOMAIN,
			'validation' => array (
				array (
					'filter' => 'eduPersonPrincipalName=%{val}',
					'result' => 0,
					'msg' => 'This identifier is already used.'
				)
			),
                        view => 1,
			'rights' => array(
				'admin' => 'w'
			),
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannAliasLogin' => array (
			'label' => 'Alias login',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'validation' => array (
				array (
					'filter' => '(|(uid=%{val})(supannAliasLogin=%{val}))',
					'result' => 0,
					'msg' => 'This login is already used.'
				)
			),
			'rights' => array(
                                'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannRefId' => array (
			'label' => 'Identifier reference',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'create' => 1,
				'modify' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'givenName' => array (
			'label' => 'First Name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array (
				'cn',
				'displayName'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'sn' => array (
			'label' => 'Name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array (
				'cn',
				'displayName'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'cn' => array (
			'label' => 'Full Name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'generate_function' => 'generate_cn',
                        'views' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'form' => array (
				'modify' => 1,
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'displayName' => array (
			'label' => 'Display name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'generate_function' => 'generate_displayName',
                        'views' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonNickname' => array (
			'label' => 'Nickname',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'telephoneNumber' => array (
			'label' => 'Phone',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannAutreTelephone' => array (
			'label' => 'Other phones',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'mobile' => array (
			'label' => 'Mobile phone',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'facsimileTelephoneNumber' => array (
			'label' => 'Fax',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'mail' => array (
			'label' => 'E-mail',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'check_data' => array (
				'email' => array(
					'msg' => "Given email address is invalid.",
				),
			),
			'validation' => array (
				array (
					'filter' => '(&(!(uid=%{uid}))(|(mail=%{val})(supannAutreMail=%{val})(supannMailPerso=%{val})))',
					'result' => 0,
					'msg' => 'This mail is already used.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannAutreMail' => array (
			'label' => 'Alternate e-mail',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'multiple' => 1,
			'check_data' => array (
				'email' => array(
					'msg' => "Given email address is invalid.",
				),
			),
			'validation' => array (
				array (
					'filter' => '(&(!(uid=%{uid}))(|(mail=%{val})(supannAutreMail=%{val})(supannMailPerso=%{val})))',
					'result' => 0,
					'msg' => 'This mail is already used.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'mailForwardingAddress' => array (
			'label' => 'Forwarding e-mail',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'check_data' => array (
				'email' => array(
					'msg' => "Given email address is invalid.",
				),
			),
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannMailPerso' => array (
			'label' => 'E-mail perso',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'check_data' => array (
				'email' => array(
					'msg' => "Given email address is invalid.",
				),
			),
			'validation' => array (
				array (
					'filter' => '(&(!(uid=%{uid}))(|(mail=%{val})(supannAutreMail=%{val})(supannMailPerso=%{val})))',
					'result' => 0,
					'msg' => 'This mail is already used.'
				)
			),
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'postalAddress' => array (
			'label' => 'Address',
			'ldap_type' => 'postalAddress',
			'html_type' => 'textarea',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannCivilite' => array (
			'label' => 'Civility',
			'ldap_type' => 'ascii',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => array(
					'M.' => 'Mr',
					'Mme' => 'Mrs.',
					'Mlle' => 'Miss.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuId' => array (
			'label' => 'Student number',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannCodeINE' => array (
			'label' => 'INE Code',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'supannEmpId' => array (
			'label' => 'Employee number',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEmpCorps' => array (
			'label' => "Corps d'appartenance",
			'ldap_type' => 'ascii',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => $BCN_N_CORPS
			),
			'multiple' => 0,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtablissement' => array (
			'label' => 'Institute',
			'ldap_type' => 'ascii',
			'html_type' => 'supannEtablissement',
			'required' => 1,
                        'check_data' => array (
                                'regex' => array(
                                        'msg' => "You must set a labeled value. ex. : {UAI}7EY2344",
                                        'params' => array('regex' => "/^{[^}]+}.*$/"),
                                ),
                        ),
			'default_value' => '{UAI}' . LS_SUPANN_ETABLISSEMENT_UAI,
			'rights' => array(
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
			),
			'dependAttrs' => array(
				'eduPersonOrgDN'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannListeRouge' => array (
			'label' => 'Red list',
			'ldap_type' => 'boolean',
			'ldap_options' => array(
				'true_value' => 'TRUE',
				'false_value' => 'FALSE'
			),
			'html_type' => 'boolean',
			'required' => 1,
			'default_value' => 'no',
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonAffiliation' => array (
			'label' => 'Affiliate',
			'ldap_type' => 'numeric',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => $liste_affiliations
			),
			'multiple' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonPrimaryAffiliation' => array (
			'label' => 'Primary affiliate',
			'ldap_type' => 'numeric',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => $liste_affiliations
			),
			'multiple' => 0,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

                /* ----------- start -----------*/
                'labeledURI' => array (
                        'label' => "Personal website",
                        'ldap_type' => 'ascii',
                        'html_type' => 'url',
                        'multiple' => 1,
                        'rights' => array(
                                'admin' => 'w',
                                'self' => 'w'
                        ),
                        'view' => 1,
                        'form' => array (
                                'modify' => 1,
                                'create' => 1
                        )
                ),
                /* ----------- end -----------*/

		/* ----------- start -----------*/
		'preferredLanguage' => array (
			'label' => 'Preferred language',
			'ldap_type' => 'ascii',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => $liste_preferredLanguage
			),
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'userCertificate' => array (
			'label' => 'Certificate',
			'help_info' => 'X.509 certificate of the person.',
			'ldap_type' => 'ascii',
			'html_type' => 'textarea',
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonOrgDN' => array (
			'label' => 'Institute',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'generate_function' => 'generate_eduPersonOrgDN',
			'rights' => array(
				'admin' => 'w'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEntiteAffectation' => array (
			'label' => 'Assignment',
			'ldap_type' => 'ascii',
			'html_type' => 'select_object',
			'html_options' => array(
				'selectable_object' => array(
					'object_type' => 'LSsupannEntite',
					'value_attribute' => 'supannCodeEntite'
				)
			),
			'multiple' => 1,
			'validation' => array (
				array (
					'msg' => "This entity doesn't exist.",
					'object_type' => 'LSsupannEntite',
					'filter' => 'supannCodeEntite=%{val}',
					'result' => 1
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array(
				'eduPersonOrgUnitDN'
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'eduPersonOrgUnitDN' => array (
			'label' => 'Assignment (DN)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'generate_function' => 'generate_eduPersonOrgUnitDN',
			'rights' => array(
				'admin' => 'w'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEntiteAffectationPrincipale' => array (
			'label' => 'Primary assignment',
			'ldap_type' => 'ascii',
			'html_type' => 'select_object',
			'html_options' => array(
				'selectable_object' => array(
					'object_type' => 'LSsupannEntite',
					'value_attribute' => 'supannCodeEntite'
				)
			),
			'multiple' => 0,
			'validation' => array (
				array (
					'msg' => "This entity doesn't exist.",
					'object_type' => 'LSsupannEntite',
					'filter' => 'supannCodeEntite=%{val}',
					'result' => 1
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array(
				'eduPersonPrimaryOrgUnitDN'
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'eduPersonPrimaryOrgUnitDN' => array (
			'label' => 'Primary assignment',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 0,
			'generate_function' => 'generate_eduPersonPrimaryOrgUnitDN',
			'rights' => array(
				'admin' => 'w'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannParrainDN' => array (
			'label' => 'Godfather',
			'help_info' => 'Godfather of this person in directory.',
			'ldap_type' => 'ascii',
			'ldap_options' => array (
				'LSobjectType' => 'LSsupannPerson',
				'displayFormat' => '%{displayName}'
			),
			'html_type' => 'select_object',
			'html_options' => array(
				'selectable_object' => array(
					'object_type' => 'LSsupannPerson',
					'value_attribute' => 'dn'
				)
			),
			'multiple' => 1,
			'validation' => array (
				array (
					'msg' => "This person doesn't exist.",
					'basedn' => '%{val}',
					'result' => 1
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannActivite' => array (
			'label' => 'Activities',
			'ldap_type' => 'ascii',
			'html_type' => 'supannLabeledValue',
                        'check_data' => array (
                                'regex' => array(
                                        'msg' => "You must set a labeled value. ex. : {SUPANN}S023",
                                        'params' => array('regex' => "/^{[^}]+}.*$/"),
                                ),
                        ),
			'multiple' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'supannRoleEntite' => array (
			'label' => 'Roles in entities',
			'ldap_type' => 'ascii',
			'html_type' => 'supannRoleEntite',
			'multiple' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannRoleGenerique' => array (
			'label' => 'Roles in entities : List of roles',
			'ldap_type' => 'ascii',
			'html_type' => 'supannRoleGenerique',
			'multiple' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannTypeEntiteAffectation' => array (
			'label' => "Roles in entities : List of entities's type",
			'ldap_type' => 'ascii',
			'html_type' => 'supannTypeEntite',
			'multiple' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'description' => array (
			'label' => 'Admin comment',
			'ldap_type' => 'ascii',
			'html_type' => 'textarea',
			'multiple' => 1,
			'rights' => array(
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

	) // Fin args
);
?>
