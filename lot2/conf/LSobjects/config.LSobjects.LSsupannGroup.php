<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

$GLOBALS['LSobjects']['LSsupannGroup'] = array (
  'objectclass' => array(
    'groupOfNames',
    'supannGroupe'
  ),
  'rdn' => 'cn',
  'container_dn' => 'ou=groups',
  'display_name_format' => '%{cn}',
  'label' => 'Groups',

  'LSaddons' => array (
    'supann',
  ),

  'LSsearch' => array (
    'attrs' => array (
      'cn',
      'description',
      'member',
    ),
  ),

  'attrs' => array (
  
    /* ----------- start -----------*/
    'cn' => array (
      'label' => "Name",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
          'filter' => 'cn=%{val}',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w',
        'reader' => 'r',
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'member' => array (
      'label' => 'Members (DN)',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannPerson',
          'value_attribute' => 'dn',
	)
      ),
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'multiple' => 0,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'owner' => array (
      'label' => 'Owner (DN)',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannPerson',
          'value_attribute' => 'dn',
	)
      ),
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'supannGroupeAdminDN' => array (
      'label' => 'Admin (DN)',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannPerson',
          'value_attribute' => 'dn',
	)
      ),
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'supannGroupeLecteurDN' => array (
      'label' => 'Reader (DN)',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannPerson',
          'value_attribute' => 'dn',
	)
      ),
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'supannGroupeDateFin' => array (
      'label' => 'Group End Date',
      'ldap_type' => 'date',
      'html_type' => 'date',
      'html_options' => array(
        'date' => array(
          'time' => 1,
	)
      ),
      'multiple' => 0,
      'rights' => array(
        'admin' => 'w',
        'owner' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'supannRefId' => array (
        'label' => 'Identifier reference',
        'ldap_type' => 'ascii',
        'html_type' => 'text',
        'rights' => array(
            'admin' => 'w',
            'owner' => 'w',
        ),
        'multiple' => 1,
        'view' => 1,
        'form' => array (
            'create' => 1,
            'modify' => 1
        )
    ),
    /* ----------- end -----------*/
  )
);
?>
