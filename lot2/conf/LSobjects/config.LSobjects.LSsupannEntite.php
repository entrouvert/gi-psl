<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

$GLOBALS['LSobjects']['LSsupannEntite'] = array (
  'objectclass' => array(
    'supannEntite',
    'organizationalUnit'
  ),
  'rdn' => 'supannCodeEntite',
  'container_dn' => 'ou=structures',
  'display_name_format' => '%{description}',
  'label' => 'Entities',

  // LSform
  'LSform' => array (
    'ajaxSubmit' => 1,
    // Layout
    'layout' => array (
      'infos' => array(
        'label' => 'Informations',
        'args' => array (
          'description',
          'ou',
          'supannTypeEntite',
          'supannCodeEntite',
          'supannRefId',
          'supannCodeEntiteParent',
        )
      ),
      'com' => array (
        'label' => 'Communication',
        'args' => array (
          'postalAddress',
          'telephoneNumber',
          'facsimileTelephoneNumber',
          'labeledURI',
        )
      )
    )
  ),

	// LSrelation
	'LSrelation' => array(
		'children' => array(
			'label' => 'Children entities',
			'emptyText' => "Have any child entity.",
			'LSobject' => 'LSsupannEntite',
			'list_function' => 'listChildren',
			'getkeyvalue_function' => 'getEntiteKeyValue',
			'update_function' => 'updateChildren',
			'remove_function' => 'deleteOneChild',
			'rename_function' => 'renameOneChildren',
			'canEdit_function' => 'canEditChildren',
			'canEdit_attribute' => 'supannCodeEntiteParent',
			'rights' => array(
				'admin' => 'r'
			)
		)
	),

  'LSsearch' => array (
    'attrs' => array (
      'ou',
      'description',
      'supannCodeEntite',
      'supannRefId',
      'telephoneNumber',
      'facsimileTelephoneNumber',
      'labeledURI',
      'mail',
      'postalAddress'
    ),
  ),

  'attrs' => array (
  
    /* ----------- start -----------*/
    'ou' => array (
      'label' => "Short name",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
	  'object_type' => 'LSsupannEntite',
          'filter' => 'ou=%{val}',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannCodeEntiteParent' => array (
      'label' => 'Parent entity',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannEntite',
          'display_name_format' => '%{description}',
          'value_attribute' => 'supannCodeEntite'
	)
      ),
      'multiple' => 0,
      'validation' => array (
        array (
          'msg' => "This entity doesn't exist.",
          'object_type' => 'LSsupannEntite',
          'filter' => 'supannCodeEntite=%{val}',
          'result' => 1
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/


    /* ----------- start -----------*/
    'supannCodeEntite' => array (
      'label' => 'Entity identifier',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'multiple' => 0,
      'validation' => array (
        array (
          'msg' => "This identifier is already affected.",
          'object_type' => 'LSsupannEntite',
          'filter' => '(supannCodeEntite=%{val})',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'multiple' => 0,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'facsimileTelephoneNumber' => array (
      'label' => 'Fax',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'mail' => array (
      'label' => 'E-mail',
      'ldap_type' => 'ascii',
      'html_type' => 'mail',
      'required' => 0,
      'check_data' => array (
        'email' => array(
          'msg' => "Given email address is invalid.",
        ),
      ),
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'postalAddress' => array (
      'label' => 'Address',
      'ldap_type' => 'postalAddress',
      'html_type' => 'textarea',
      'required' => 0,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'telephoneNumber' => array (
      'label' => 'Phone',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'labeledURI' => array (
      'label' => "Personal website",
      'ldap_type' => 'ascii',
      'html_type' => 'url',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w',
        'companyadmin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannRefId' => array (
      'label' => 'Identifier reference',
      'ldap_type' => 'ascii',
      'html_type' => 'supannLabeledValue',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannTypeEntite' => array (
      'label' => "Entity type",
      'ldap_type' => 'ascii',
      'html_type' => 'supannTypeEntite',
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1
      )
    ),
    /* ----------- end -----------*/


  )
);
?>
