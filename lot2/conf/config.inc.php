<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

// Configuration LDAP Saisie :
$GLOBALS['LSconfig'] = array(
  'NetLDAP2' => '/usr/share/php/Net/LDAP2.php',
  'Smarty' => '/usr/share/php/smarty3/Smarty.class.php',
  'lang' => 'fr_FR',
  'encoding' => 'UTF8',
  'cacheLSprofiles' => true,
  'cacheSubDn' => true,
  'cacheSearch' => true,
  'keepLSsessionActive' => true,
  'ldap_servers' => array (
    array (
      'name' => 'LSexample',
      'ldap_config'=> array(
            'host'     => '192.168.43.23',
            'port'     => 389,
            'version'  => 3,
            'starttls' => false,
            'binddn'   => 'uid=admin,ou=people,dc=sorbine,dc=fr',
            'bindpw'   => '',
            'basedn'   => 'dc=sorbine,dc=fr',
            'options'  => array(),
            'filter'   => '(objectClass=*)',
            'scope'    => 'sub'
      ),
      'LSprofiles' => array (
        'admin' => array (
          'dc=sorbine,dc=fr' => array (
            'cn=admin,ou=groups,dc=sorbine,dc=fr' => array (
              'attr_value' => '%{dn}',
              'attr' => 'member',
              'LSobject' => 'LSsupannGroup'
            )
          ),
//          'LSobjects' => array (
//             'LSsupannGroup' => array(
//                 'attr' => 'supannGroupeAdminDN',
//                 'attr_value' => '%{dn}',
//             )
//          )
         ),
         'owner' => array (
           'LSobjects' => array (
              'LSsupannGroup' => array(
                  'attr' => 'owner',
                  'attr_value' => '%{dn}',
              )
           )
         ),
         'reader' => array (
           'LSobjects' => array (
              'LSsupannGroup' => array(
                  'attr' => 'supannGroupeLecteurDN',
                  'attr_value' => '%{dn}',
              )
           )
         )
      ),
      'cacheLSprofiles' => false,
      'cacheSearch' => true,
      'authObjectType' => 'LSsupannPerson',
      'authObjectFilter' => '(|(uid=%{user})(mail=%{user})(supannAliasLogin=%{user}))',
      'authObjectTypeAttrPwd' => 'userPassword',
      'LSaccess' => array(
        'LSsupannEntite',
        'LSsupannPerson',
        'LSsupannGroup'
      ),
    ),
  )
);

// Interface
// Theme Black
//define('LS_THEME','black');
//define('LS_TEMPLATES_DIR', 'templates/default');

// Theme Default
define('LS_THEME','default');
define('LS_TEMPLATES_DIR', 'templates');
define('LS_IMAGES_DIR', 'images');
define('LS_CSS_DIR', 'css');

//Debug
define('LSdebug',false);

// Logs
$GLOBALS['LSlog']['filename'] = '/var/log/ldapsaisie/ldapsaisie.log';
$GLOBALS['LSlog']['enable'] = true;

define('NB_LSOBJECT_LIST',20);
define('NB_LSOBJECT_LIST_SELECT',11);

define('MAX_SEND_FILE_SIZE',2000000);


// Javascript
$GLOBALS['defaultJSscipts']=array(
  'mootools-core.js',
  'mootools-more.js',
  'functions.js',
  'LSdefault.js',
  'LSinfosBox.js'
);

?>
