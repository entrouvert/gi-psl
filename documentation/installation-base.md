% Gestion d'identités PSL -- Installation de base
% Entr'ouvert SCOP -- http://www.entrouvert.com

Matériel et infrastructure requis
=================================

Note : pour simplifier la mise en place d'un test, les composants du système peuvent s'installer sur une machine unique. Cependant, pour la recette et surtout la production, les composants doivent être installés sur des machines isolées.

Nous préconisons l'installation sur *machine virtuelle*. Tous les composants fonctionnent sur n'importe quelle machine virtuelle compatible Debian 7 (VMware, VirtualBox, kvm, etc).

Configuration réseau commune
----------------------------

* L'installation est plus simple sur un réseau piloté par *DHCP*;
* Les machines doivent avoir accès à Internet, au moins DNS et HTTP, pour télécharger les paquets logiciels de la solution puis leur mise à jour;
* Les machines doivent disposer d'entrées DNS, par exemple `ldap.example.net`, `ldapsaisie.example.net`, `authentic.example.net`

Serveur LDAP
------------

Caractéristiques minimales :

* Processeur : Intel ou AMD64, *64 bits* (architecture nommée «amd64» pour Linux) 2 GHz monocoeur 
* Mémoire vive: 2 Go
* Disque : 10 Go

Réseau :

* En entrée : accès LDAP (389/tcp)
* En sortie : DNS, web (pour mises à jour)

Note : il est possible d'instancier plusieurs serveurs LDAP ayant tous la même configuration matérielle.

Serveur interface de gestion du LDAP
------------------------------------

Caractéristiques minimales :

* Processeur : Intel ou AMD64, *64 bits* (architecture nommée «amd64» pour Linux) 2 GHz monocoeur 
* Mémoire vive: 2 Go
* Disque : 5 Go

Réseau :

* En entrée : accès HTTPS (443/tcp)
* En sortie : LDAP vers le(s) serveur(s) LDAP, DNS, web (pour mises à jour)


Serveur IdP
-----------

Caractéristiques minimales :

* Processeur : Intel ou AMD64, *64 bits* (architecture nommée «amd64» pour Linux) 2 GHz monocoeur 
* Mémoire vive: 2 Go
* Disque : 5 Go

Réseau :

* En entrée : accès HTTPS (443/tcp)
* En sortie : LDAP vers le(s) serveur(s) LDAP, DNS, web HTTP et HTTPS (**chargement des métadonnées de fédération** et mises à jour logicielles)


Image ISO commune
=================

Toutes les machines s'installent depuis la même image ISO. Télécharger le fichier `.iso` le plus récent disponible sur :

> [http://deb.entrouvert.org/supann/](http://deb.entrouvert.org/supann/)

Amorçage sur l'image ISO
========================

Pour lancer l'installation, il faut amorcer (_booter_) la machine choisie sur
l'image ISO ; suivre pour cela la procédure de la machine, virtuelle ou non.

**Attention: l'installation effacera toutes les données présentes sur la machine.**

\ ![images/psl-boot-iso.png](images/psl-boot-iso.png)

Choisir le mode d'installation de base: **`SupAnn base Install`**

L'installation est celle d'une distribution Debian où la plupart des réponses ont déjà été faites. Les grandes étapes sont :

1. Configuration des paramètres réseau si la machine n'arrive pas à s'autoconfigurer (par exemple, pas de DHCP)
1. Choix d'un mot de passe administrateur (utilisateur _root_)
1. Partitionnement : sauf si vous avez des besoins spécifiques, laisser le choix par défaut *«Assisté - utiliser un disque entier»*
1. Configuration d'un mandataire HTTP (_proxy_)
1. Installation du système proprement dit (entre 2 et 10 minutes, selon la bande passante disponible et la rapidité de la machine)
1. Après la fin de l'installation, redémarrage (l'image ISO est retirée automatiquement du _boot_)

\ ![images/psl-iso-installee.png](images/psl-iso-installee.png)

Premier démarrage
-----------------

Après le démarrage, se connecter en tant que `root` avec le mot de passe décidé lors de l'installation :

\ ![images/psl-premier-login.png](images/psl-premier-login.png)


Horloge synchronisée
====================

Il est très important de faire en sorte que toutes les machines de la solution
soient à l'heure. Pour cela, vous pouvez installer le paquet `ntp`:

	# apt-get install ntp

Éventuellement, si vous disposez d'un serveur NTP local, vous pouvez l'indiquer
dans le fichier `/etc/ntp.conf`. Par défaut, `ntp` utilise les serveurs du
projet Debian (`*.debian.pool.ntp.org`).

Note : le paquet `ntp` n'est pas installé par défaut car certains systèmes de
virtualisation peuvent proposer une horloge système déjà synchronisée par la
machine hôte.


Annexes
=======

Ajout d'outils supplémentaires
------------------------------

Par défaut, des outils Unix simples et relativement _user-friendly_ sont
installés, tels que l'éditeur `nano` pour la modification des fichiers textes.

Pour les utilisateurs habitués à l'utilisation de machines GNU/Linux, et
notamment Debian GNU/Linux, il est possible d'installer des composants
d'administration qui aideront à la gestion quotidienne des machines. Tous les
logiciels de **Debian GNU/Linux version 7 (Wheezy)** sont installables. Par
exemple :

* `apt-get install vim` et/ou`apt-get install emacs`
* `apt-get install ssh`
* `apt-get install screen`
* etc.

Installation sur une machine Debian GNU/Linux 7 (Wheezy)
--------------------------------------------------------

Au lieu de passer par l'installation de l'image ISO pré-configurée, il est
possible d'installer la solution sur une machine Debian GNU/Linux « pure ».

Pour mieux connaître le système Debian, le livre libre « _Le cahier de
l'administrateur Debian_ » est un excellent point de démarrage. Il est
disponible sur [http://debian-handbook.info/](http://debian-handbook.info/) en plusieurs langues et plusieurs
versions.

Une fois l'installation de Debian 7 terminée, il faut ajouter les dépôts APT
suivants :

> `deb http://deb.entrouvert.org/ wheezy-supann main`

> `deb http://deb.entrouvert.org/ wheezy main`

et la clé associée `http://deb.entrouvert.org/entrouvert.gpg`

_Note: l'installation sur une machine Debian avec d'autres sources n'est pas
supportée._


-----

Historique du document
======================

> 20150217 tnoel -- première version

